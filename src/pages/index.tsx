import { useState,useEffect } from 'react';
import axios from 'axios';

export default function Home() {
  const [todo,setTodo] = useState("");

  const [todos,setTodos] = useState([]);
  const [tmpFilter, setTmpFilter ] = useState([]);

  const [isEdit,setIsEdit] = useState(false);
  const [tmpItem, setTmp] = useState<any>({});
  const baseUrl = "http://178.128.61.86:5000/api/";


  useEffect(() => {
    getTodos();
  }, []);


  const getTodos = async () => {
    await axios.get(baseUrl+"todo").then((response:any) => {
      setTodos(response?.data);
    });
  }

  const addTodo = () => {

    if(isEdit){
      tmpItem.todo = todo;
      EditTodo(tmpItem);
      setTodo('');
      setIsEdit(false);
    }
    else{
      axios.post(baseUrl+"todo",
      {
        todo: todo,
        isCompleted: false
      }
      ).then((response:any) => {
        setTodos([...todos, response.data] as any);
        setTodo('');
      });
    }
    
  }

  const EditTodo = async (data:any) =>{
    MarkTodo(data);
  }

  const MarkTodo = async (data:any) => {
    axios.put(baseUrl+`todo/${data?.id}`,
    data
    ).then((response:any) => {
      getTodos();
    });
  };

  const DeleteTodo = (id:any) =>{
    axios.delete(baseUrl+`todo/${id}`).then(() => {
      setTodos(todos.filter((todo:any) => todo?.id !== id));
    });
  }

  const handleValue = () =>{
    if(!todo){
      alert("Please write todo");
      return
    }
    const duplicate = todos.find((row:any) => row?.todo === todo);
    if (duplicate) {
      alert('Todo already exists!');
      return;
    }
    else{
      addTodo();
    }
  }

  const filter = todos.filter((row:any) => row?.todo.toLowerCase().includes(todo.toLowerCase()));
  
  return (
    <div className='w-full lg:w-2/4 m-2 p-2 bg-white mx-auto rounded-md conatiner-box'>
      
      <section >
        <h1 className='text-center text-2xl'>Todo List Application</h1>
        <div className="border-b my-4 mb-6 mx-2"></div>
      </section>

      <section>
        <div className='flex'>
          <input value={todo} type="text" placeholder='Write Your Todo'  onChange={(e) => { setTodo(e.target.value)}}  onKeyUp={(event) => event.key === 'Enter' && handleValue()}  className='w-full px-2 py-1 mx-2 border rounded-md ' />
          <button onClick={handleValue} className='bg-sky-700 text-white px-2 py-2 rounded-md'>Save</button>
        </div>
      </section>

      <section>
        <div>
          <ul className='mt-5 px-2'>

            {filter.map((row:any,index) => (
              <li  key={row?.id} className='hover:shadow-md block lg:flex justify-between cursor-pointer duration-50 border p-2 mb-3 rounded-md w-full'>
              
                <span style={{ textDecoration: row.isCompleted ? 'line-through' : 'none' }}>{row?.todo}</span>
                
                <div className='option hidden'>
                  <button onClick={()=> MarkTodo({id:row?.id, todo: row?.todo, isCompleted:true, created_at: row?.created_at})} className=' ml-0 mt-2 lg:mt-0  lg:ml-2 bg-blue-300  px-1.5 rounded-md'>
                      <p>Complete </p>
                  </button>
                  <button onClick={()=> MarkTodo({id:row?.id, todo: row?.todo, isCompleted:false, created_at: row?.created_at})} className='ml-2 bg-blue-300  px-1.5 rounded-md'>
                      <p>Incomplete </p>
                  </button>
                  <button onClick={()=>{setTodo(row?.todo);setIsEdit(true); setTmp(row)}} className='ml-2 bg-blue-300  px-1.5 rounded-md'>Edit</button>
                  <button onClick={()=> DeleteTodo(row?.id)} className='ml-2 bg-green-500  px-1.5 rounded-md'>Delete</button>
                </div>
              </li>
            ))}

          </ul>

          {filter.length === 0 && todo.trim() !== '' && (
            <p className='text-center'>No result</p>
          )}
        </div>
      </section>

    </div>

  )
}
